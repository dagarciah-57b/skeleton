package org.example.unit.user.command;

import org.example.user.User;
import org.example.user.port.repository.UserRepository;
import org.example.user.service.CreateUserService;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class CreateUserServiceTest {
    private UserRepository repository;
    private CreateUserService subject;

    @Before
    public void arrange() {
        repository = mock(UserRepository.class);
        subject = new CreateUserService(repository);
    }

    @Test
    public void success_when_user_not_exists() {
        // arrange
        User user = User.builder().name("username").build();

        // act
        subject.execute(user);

        // assert
        verify(repository, times(1)).exists(user);
        verify(repository, times(1)).save(user);
    }
}

package org.example.user.exception;

import org.example.user.User;

public class UserAlreadyExists extends RuntimeException {
    public UserAlreadyExists(User user) {
        super(String.format("User with name %s already exists", user.getName()));
    }
}

package org.example.user.service;

import org.example.user.User;
import org.example.user.exception.UserAlreadyExists;
import org.example.user.port.repository.UserRepository;

public class CreateUserService {
    private final UserRepository repository;

    public CreateUserService(UserRepository repository) {
        this.repository = repository;
    }

    public void execute(User user) {
        if (repository.exists(user)) {
            throw new UserAlreadyExists(user);
        }

        repository.save(user);
    }
}

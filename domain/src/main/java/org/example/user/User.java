package org.example.user;

import lombok.*;

@Getter
public class User {
    private final String name;

    @Builder
    public User(String name) {
        this.name = name;
    }
}

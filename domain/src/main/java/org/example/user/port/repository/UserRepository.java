package org.example.user.port.repository;

import org.example.user.User;

import java.util.List;

public interface UserRepository {
    List<User> findAll();

    void save(User user);

    boolean exists(User user);
}

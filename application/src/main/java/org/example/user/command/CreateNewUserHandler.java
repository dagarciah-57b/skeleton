package org.example.user.command;

import org.example.common.command.CommandResponse;
import org.example.user.User;
import org.example.user.service.CreateUserService;

public class CreateNewUserHandler {
    private final CreateUserService service;

    public CreateNewUserHandler(CreateUserService service) {
        this.service = service;
    }

    public CommandResponse<User> execute(CreateUserCommand command) {
        User user = UserFactory.create(command);
        service.execute(user);
        return CommandResponse.of(user);
    }
}

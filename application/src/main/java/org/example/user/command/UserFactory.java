package org.example.user.command;

import org.example.user.User;

public interface UserFactory {
    static User create(CreateUserCommand command) {
        return User.builder()
                .name(command.getName())
                .build();
    }
}

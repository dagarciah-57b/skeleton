package org.example.user.query;

import org.example.user.User;
import org.example.user.port.repository.UserRepository;

import java.util.List;

public final class GetAllUsersHandler {
    private final UserRepository repository;

    public GetAllUsersHandler(UserRepository repository) {
        this.repository = repository;
    }

    public List<User> handle() {
        return repository.findAll();
    }
}

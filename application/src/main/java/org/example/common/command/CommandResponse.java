package org.example.common.command;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(staticName = "of")
public class CommandResponse<T> {
    private final T content;
}

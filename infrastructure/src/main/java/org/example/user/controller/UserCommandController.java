package org.example.user.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.example.common.command.CommandResponse;
import org.example.user.User;
import org.example.user.command.CreateNewUserHandler;
import org.example.user.command.CreateUserCommand;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@Api(tags = "User commands")
public class UserCommandController {
    private final CreateNewUserHandler createNewUserHandler;

    public UserCommandController(CreateNewUserHandler createNewUserHandler) {
        this.createNewUserHandler = createNewUserHandler;
    }

    @PostMapping
    @ApiOperation("Try to create a new user")
    public CommandResponse<User> create(@RequestBody CreateUserCommand command) {
        return createNewUserHandler.execute(command);
    }
}

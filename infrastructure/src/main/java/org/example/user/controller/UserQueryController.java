package org.example.user.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.example.user.User;
import org.example.user.query.GetAllUsersHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
@Api(tags = "User queries")
public class UserQueryController {
    private final GetAllUsersHandler getAllUsersHandler;

    public UserQueryController(GetAllUsersHandler getAllUsersHandler) {
        this.getAllUsersHandler = getAllUsersHandler;
    }

    @GetMapping
    @ApiOperation("Listing existing users")
    public List<User> all(){
        return getAllUsersHandler.handle();
    }
}

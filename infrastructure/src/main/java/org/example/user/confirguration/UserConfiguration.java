package org.example.user.confirguration;

import org.example.user.command.CreateNewUserHandler;
import org.example.user.query.GetAllUsersHandler;
import org.example.user.port.repository.UserRepository;
import org.example.user.service.CreateUserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserConfiguration {
    @Bean
    GetAllUsersHandler getAllUsersHandler(UserRepository repository) {
        return new GetAllUsersHandler(repository);
    }

    @Bean
    CreateUserService createUserService(UserRepository repository) {
        return new CreateUserService(repository);
    }

    @Bean
    CreateNewUserHandler createNewUserHandler(CreateUserService createUserService) {
        return new CreateNewUserHandler(createUserService);
    }
}

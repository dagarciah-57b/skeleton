package org.example.user.adapter.repository;

import org.example.user.User;
import org.example.user.port.repository.UserRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@ConditionalOnMissingBean(type = "UserRepository")
public class UserInMemoryRepository implements UserRepository {
    private final Map<String, User> storage = new HashMap<>();

    @Override
    public List<User> findAll() {
        return new ArrayList<>(storage.values());
    }

    @Override
    public void save(User user) {
        storage.put(user.getName(), user);
    }

    @Override
    public boolean exists(User user) {
        return storage.containsKey(user.getName());
    }
}

package org.example.user.adapter.repository;

import org.example.user.User;
import org.example.user.port.repository.UserRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;


@Primary
@Component
public class UserSqlRepository implements UserRepository {

    private final JdbcTemplate jdbc;

    public UserSqlRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public List<User> findAll() {
        return jdbc.query("select * from transactional.\"user\"", new UserRowMapper());
    }

    @Override
    public void save(User user) {
        // Pending implementation
    }

    @Override
    public boolean exists(User user) {
        return false;
    }

}

# Skeleton
Define a purpose of scaffolding to build a microservice application based on clean architecture. This main project contains three sub-modules:
### Domain
Represents the layer that contains all business model, ports and services to resolve the specific domain goals. The application layer can access it, and it must not access to another layer or third-party libraries.
<pre>
Example:
|-- user
   |-- exception
   |-- port
      |-- repository
   |-- service
</pre>

### Application
In this layer, you can implement the use cases using the domain definitions to do it. In this case the application layer implements separated command and queries requests of user. 
<pre>
Example:
|-- user
   |-- command
   |-- query
</pre>

### Infrastructure.  
The application layer, contains all implementations of ports defined on domain layer. In this layer you can free to access to third-party libraries. In this case, this layer has the configuration of the self-contained application implemented using SpringBoot.
<pre>
Example:
|-- user
   |-- adapter
      |-- repository
   |-- configuration
   |-- controller
|-- swagger
   |-- configuration
|-- Application.java
</pre>

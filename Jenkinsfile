pipeline {
  //Donde se va a ejecutar el Pipeline
  agent {
    label 'master'
  }

  //Opciones específicas de Pipeline dentro del Pipeline
  options {
    buildDiscarder(logRotator(numToKeepStr: '3'))
    disableConcurrentBuilds()
  }

  //Una sección que define las herramientas “preinstaladas” en Jenkins
  tools {
    jdk 'jdk-1.8.221'
    maven 'maven-3.8.3'
  }

  //Aquí comienzan los “items” del Pipeline
  stages{
    stage('Checkout') {
      steps{
        echo "------------>Checkout<------------"
        checkout([
            $class: 'GitSCM',
            branches: [[name: '*/master']],
            doGenerateSubmoduleConfigurations: false,
            extensions: [],
            gitTool: 'Default',
            submoduleCfg: [],
            userRemoteConfigs: [[
            credentialsId: 'david.alejandro@57blocks.com',
            url:'https://dagarciah-57b@bitbucket.org/dagarciah-57b/skeleton.git'
            ]]
        ])
      }
    }

    stage('Clean') {
      steps{
        echo "------------>Clean<------------"
        sh 'mvn clean'
      }
    }

    stage('Compile & Unit Tests') {
      steps{
        echo "------------>Compile & Unit Tests<------------"
        sh 'mvn compile -DskipTests'
        sh 'mvn test'
      }
    }

    stage('Static Code Analysis') {
      steps{
        echo '------------>Análisis de código estático<------------'
        withSonarQubeEnv('sonarqube-scanner') {
          sh "${tool name: 'sonarqube-scanner', type:'hudson.plugins.sonar.SonarRunnerInstallation'}/bin/sonar-scanner -Dproject.settings=sonar-project.properties"
        }
      }
    }
  }
}
